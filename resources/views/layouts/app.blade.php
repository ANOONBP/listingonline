<!DOCTYPE html>
<html lang="en">
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="desrciption" content="">
        <meta name="author" content="">
        <title>Listing Online</title>

        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
        <style>
            html,body{
                overflow: hidden;
                font-family: 'Montserrat', sans-serif;
                margin: 0;
               
            }

            html{
                background-color: rgba(0,0,0,0.05);
            }
            video{
                z-index: -1000;
                object-fit: fill;
            }

            nav{
                height: 160px;
                display: flex;
                align-items: center;
                background-color: rgba(255, 140, 212, 0.8);
                box-shadow: 0 1px 1px 1px rgba(0,0,0,0.15);
                position: fixed;
                width: 100vw;
            }

            nav > space{
                flex: 2;
            }

            nav > div{
                flex: 1;
                display: flex;
                justify-content: center;
            }

            nav > div > span, nav > div > a{
                font-size: 24px;
                color: white;
                text-decoration: none;
                cursor: pointer;
            }

            nav > div > span:hover hr, nav > div > a:hover hr{
                width: 100%;
                border-color: white;
            }


            nav > .title{
                height: 100%;
                text-align: center;
                cursor: initial;
                flex: 2;
                display:flex;
                align-items: center;
                
            }

            nav > .title > span{
                cursor: inherit;
                margin-bottom: 6px;
                font-size: 32px;
                padding: 10px 20px;
                color: white;
                background-color: black;
            }

            hr{
                margin: 2px 0;
                border: 1px solid rgba(255, 140, 212, 0.8);
                width: 0;
                transition: width 0.25s;
            }

            /* home */
            page{
                width: 60vw;
                height: auto;
                display: flex;
                flex-direction: column;
                align-items: flex;
                margin: auto;
                padding-left: 25px;
                background-color: white;
                
            }

            title{
                display: block;
                margin: 30px 0;
                font-size: 32px;
                background-color: white;
            }

            page-body{
                width: 100%;
                height: auto;
                display: flex;
                flex-direction: column;
                
            }

            page-body row{
                width: 100%;
                display: flex;
                margin: 20px 0;
                font-size: 24px;
            }

            page-body label{
                width: auto;
                text-align: end;
            }

            page-body label::after{
                content: ' :';
            }

            page-body span{
                width: 50%;
            }

            page-body span::before{
                content: ' ';
            }

            header{
                
                font-size: 32px;
                height: 150px;
                width: 60vw;
                display: flex;
                align-items: center;
                padding-left: 25px;
                margin: 25px auto;
                background-color: white;
                margin-top: 185px;
            }

            video-container{
                display: flex;
                flex-wrap: wrap;
                width: 100vw;
                height: 100vh;
            }

        </style>

    </head>

    <body>
        
        <nav>
            <space></space>
            <div><span>Home<hr></span></div>
            <div><span>Products<hr></span></div>
            <div><span>Services<hr></span></div>
            <div class='title'><span>Listing Online</span></div>
            <div><span>About<hr></span></div>
            <div><span>Contract<hr></span></div>
            @if(Session::has('sso2'))
                <div><a href="{{config('app.serverURL').'/auth/logout?redirectURL='.config('app.logoutURL')}}">Logout<hr></a></div>
            @else
                <div><a href="{{config('app.serverURL').'/auth/login?consumerKey='.config('app.consumerKey').'&redirectURL='.config('app.redirectURL')}}">Login<hr></a></div>
            @endif
            <div id='sso-login'></div>
            <space></space>
        </nav>

        @yield('content')
        <script>           
            function SSOLogin(consumerKey, redirectURL, logoutURL) {
                let hostUrl = 'http://localhost:3000';
                var http = new XMLHttpRequest();
                var url = hostUrl + '/auth/getLoginStatus?consumerKey=' + consumerKey + '&redirectURL=' + redirectURL;
                http.open('GET', url, true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.withCredentials = true;

                http.onreadystatechange = function() {
                    if(http.readyState == 4) {
                    
                        if(http.status == 200){
                            ssoLogin.textContent = 'Logout';
                            ssoLogin.onclick = function(e){
                                window.location = hostUrl + '/auth/logout?redirectURL=' + logoutURL;
                            }
                        }else{
                            ssoLogin.textContent = 'Login';
                            ssoLogin.onclick = function(e){
                                window.location = hostUrl + '/auth/login?consumerKey=' + consumerKey + '&redirectURL=' + redirectURL;

                            }
                        }
                    }
                }
            
                http.send();
                let ssoLogin = document.getElementById('sso-login');
                ssoLogin.style = 'align-items : center';
                ssoLogin.style.display = 'flex';
                ssoLogin.style.position = 'fixed';
                ssoLogin.style.height = '50px';
                ssoLogin.style.top = '10px';
                ssoLogin.style.right = '10px'
                ssoLogin.style.border = 'none';
                ssoLogin.style.backgroundColor = 'rgb(0,0,120)';
                ssoLogin.style.color = 'white';
                ssoLogin.style.fontSize = '18px';
                ssoLogin.style.padding = '0 30px';
                ssoLogin.style.cursor = 'pointer';
            }  

            var SSOLogin = new SSOLogin('8369f496-410b-4ebc-bd32-21e4a48c1dfb', 'http://127.0.0.1:8080/getCode', 'http://127.0.0.1:8080/logout');
        </script>
    </body>
</html>
