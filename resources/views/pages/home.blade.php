@extends('layouts.app')

@section('content')

<header>
    Welcome <span></span><span></span> to Rental for Holidays
</header>

<page>
    <title>User Info</title>

    <page-body>
        <row>
            <label>AccessToken </label><span>{{Session::get('sso2')['accessToken']}}</span>   
        </row>
        <row>
            <label>First name</label><span></span>   
        </row>
        <row>
            <label>Last name</label><span></span>
        </row>
        <row>
            <label>Address</label><span></span>
        </row>
        <row>
            <label>Telephone</label><span></span>
        </row>
    </page-body>
</page>

<script>
// document.getElementById('firstName2').textContent = result.firstname;
// document.getElementById('firstName').textContent = result.firstname;
// document.getElementById('lastName2').textContent = result.lastname;
// document.getElementById('lastName').textContent = result.lastname;
// document.getElementById('address').textContent = result.address;
// document.getElementById('tel').textContent = result.tel;
</script>
@endsection