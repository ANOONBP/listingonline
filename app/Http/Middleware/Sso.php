<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Route;
use Redirect;
use Request;
use Input;

class sso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Check if campaignMaster session already exist or not.
        if(Session::has('sso2')){
            // Return to next request
            return $next($request);
        }
        
        // Redirect user to campaign index view.
        return redirect()->route('login');
        
    }
}
