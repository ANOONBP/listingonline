<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
// use Illuminate\Http\Request;
use Session;
use Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        // User campaign middleware for this controller.
        $this->middleware('sso', ['except' => ['login', 'getCode'] ]);
    }

    function home(){
        if(!Session::has('sso2')){
            
            // Return to next request
            return redirect()->route('login');
        }
        return view('pages.home');
    }

    function login(){
        if(Session::has('sso2')){
            
            // Return to next request
            return redirect()->route('home');
        }
        return view('pages.login');
    }

    function logout(){
        Session::remove('sso2');
        
        return redirect()->route('login');
    }

    function getCode(){
        if(Request::get('code')){

            if(!Session::has('sso2')){
                $json = $this->curl(
                config('app.serverURL')."/auth/accesstoken", 
                    array(
                    "code" => Request::get('code'),
                    "consumerKey" => config('app.consumerKey'),
                    "consumerSecret" => config('app.consumerSecret'),
                    "redirectURL" => config('app.redirectURL'))
                );
                Session::put('sso2',  $json);
                // dd(Session::get('sso2'));
                return redirect()->route('home');
            }
            // return redirect()->route('home');
        }

    }

    function curl($CURLOPT_URL, $CURLOPT_POSTFIELDS){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$CURLOPT_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($CURLOPT_POSTFIELDS));

        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        return json_decode($server_output, true);
    }
}